use std::io::Read;

use anyhow::Context;

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    // Load config
    let config: do_not_disturb::Config = {
        let config = dirs::config_dir().context("configuration directory")?;
        let config = config.join("dnd").join("config.toml");
        let mut config = std::fs::File::open(config)
            .context("cannot open configuration file at $XDG_CONFIG_HOME/dnd/config.toml")?;

        let mut buf = Vec::new();
        let _bytes_read = config
            .read_to_end(&mut buf)
            .context("reading configuration file")?;
        let config = String::from_utf8(buf).context("parsing config as UTF-8")?;
        toml::from_str(&config)?
    };

    // Parse configs of triggers and backends
    let mut daemon = config.into_daemon().await?;

    loop {
        daemon.process_event().await?;
    }
}
