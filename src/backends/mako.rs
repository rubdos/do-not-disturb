use crate::Backend;

/// Backend for [mako](https://wayland.emersion.fr/mako/).
///
/// Make is able to be set in different *modes*, and this back-end will switch between two modes
/// for setting a do-not-disturb setting.
///
/// By default, the backend will switch between a mode named `do-not-disturb` and `default`, but
/// this is configurable.  Make sure to configure Mako with this mode, because it is not available
/// by default.
///
/// ```toml
/// [mode=do-not-disturb]
/// invisible=1
/// ```
///
/// Then, configure `dndd`.
///
/// ```toml
/// [backend.mako]
/// # These two values are defaults, so just specifying an empty [mako] section is enough.
/// dnd-mode = "do-not-disturb"
/// default-mode = "default"
/// ```
#[derive(serde::Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Mako {
    #[serde(default = "default_dnd", rename = "dnd-mode")]
    dnd_mode: String,
    #[serde(default = "default_default", rename = "default-mode")]
    default_mode: String,
}

fn default_dnd() -> String {
    "do-not-disturb".into()
}

fn default_default() -> String {
    "default".into()
}

#[async_trait::async_trait]
impl Backend for Mako {
    fn from_config(config: toml::Value) -> anyhow::Result<Self> {
        Ok(config.try_into()?)
    }

    async fn enter(&self) -> anyhow::Result<()> {
        self.set_mode(&self.dnd_mode).await
    }

    async fn leave(&self) -> anyhow::Result<()> {
        self.set_mode(&self.default_mode).await
    }
}

impl Mako {
    async fn set_mode(&self, mode: &str) -> anyhow::Result<()> {
        let mut proc = tokio::process::Command::new("makoctl")
            .arg("set-mode")
            .arg(mode)
            .spawn()
            .expect("makoctl exists");
        let status = proc.wait().await?;
        println!("`makoctl set-mode {mode}` returned {status}");
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn deserialize_default_configs() {
        let configs = [
            toml::value::Map::default().into(),
            toml::toml! {
                dnd-mode = "do-not-disturb"
                default-mode = "default"
            },
        ];
        for config in configs {
            let config = Mako::from_config(config).unwrap();

            assert_eq!(config.dnd_mode, "do-not-disturb");
            assert_eq!(config.default_mode, "default");
        }
    }
}
