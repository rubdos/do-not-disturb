use std::collections::HashSet;
use std::future::Future;
use std::pin::Pin;
use std::task::{self, Poll};

use futures::StreamExt;
use inotify::{EventStream, Inotify};

use crate::Trigger;

#[derive(serde::Deserialize)]
struct NetworkConfig {
    #[serde(default)]
    dnd_search_domains: HashSet<String>,
}

#[derive(Default)]
struct NetworkStatus {
    search_domains: HashSet<String>,
}

impl NetworkConfig {
    fn setup_inotify(&self) -> anyhow::Result<EventStream<Vec<u8>>> {
        use inotify::WatchMask;
        let mut inotify = Inotify::init()?;
        inotify.add_watch(
            "/etc/",
            WatchMask::MODIFY | WatchMask::CREATE | WatchMask::DELETE,
        )?;
        Ok(inotify.event_stream(vec![0u8; 1024])?)
    }
}

pub struct Network {
    config: NetworkConfig,

    inotify_stream: EventStream<Vec<u8>>,

    status: NetworkStatus,

    processor: Option<Pin<Box<dyn Future<Output = anyhow::Result<NetworkStatus>>>>>,
}

impl Trigger for Network {
    fn from_config(config: toml::Value) -> anyhow::Result<Self>
    where
        Self: Sized,
    {
        let config: NetworkConfig = config.try_into()?;

        let inotify_stream = config.setup_inotify()?;

        let mut netw = Network {
            config,
            inotify_stream,

            status: NetworkStatus::default(),

            processor: None,
        };
        netw.start_fetch();
        Ok(netw)
    }

    fn poll(&mut self, cx: &mut task::Context) -> task::Poll<anyhow::Result<crate::Action>> {
        if let Some(processor) = self.processor.as_mut() {
            let status = futures::ready!(Future::poll(processor.as_mut(), cx));
            self.processor = None;
            match status {
                Ok(status) => {
                    if let Some(action) = self.cas_network_status(status) {
                        return Poll::Ready(Ok(action));
                    }
                }
                Err(e) => {
                    println!("Error processing network status. Continuing. {}", e);
                }
            };
        }

        println!("Checking inotify");
        while let Some(event) = futures::ready!(self.inotify_stream.poll_next_unpin(cx)?) {
            println!("Inotify called. event: {:?}", event);
            if self.processor.is_none() && event.name.map_or(false, |x| x == "resolv.conf") {
                self.start_fetch();
                cx.waker().wake_by_ref();
            }
        }

        unreachable!("Inotify stream exhausted");
    }
}

impl Network {
    fn start_fetch(&mut self) {
        let proc = async {
            let search_domains = check_search_domains().await?.into_iter().collect();
            Ok(NetworkStatus { search_domains })
        };
        self.processor = Some(Box::pin(proc));
    }

    fn cas_network_status(&mut self, new: NetworkStatus) -> Option<crate::Action> {
        // Compare search domains
        let disappeared = self.status.search_domains.difference(&new.search_domains);
        let appeared = new.search_domains.difference(&self.status.search_domains);

        println!("Disappeared domains: {:?}", disappeared);
        println!("Appeared domains: {:?}", appeared);

        let mut action = None;

        if self
            .config
            .dnd_search_domains
            // efficiency?
            .intersection(&appeared.cloned().collect())
            .next()
            .is_some()
        {
            action = Some(crate::Action::TurnDndOn);
        } else if self
            .config
            .dnd_search_domains
            // efficiency?
            .intersection(&disappeared.cloned().collect())
            .next()
            .is_some()
        {
            // Only turn off if there's no reason for turn on.
            action = Some(crate::Action::TurnDndOff);
        }

        self.status = new;

        action
    }
}

async fn check_search_domains() -> anyhow::Result<Vec<String>> {
    use tokio::io::AsyncBufReadExt;

    let mut domains = Vec::new();

    // XXX refactor filename
    let f = tokio::fs::File::open("/etc/resolv.conf").await?;
    let f = tokio::io::BufReader::new(f);
    let mut lines = f.lines();
    while let Some(line) = lines.next_line().await? {
        if line.starts_with("search") {
            let mut line = line.split_ascii_whitespace();
            assert_eq!("search", line.next().expect("non-empty line"));

            for el in line {
                domains.push(el.to_string());
            }
        }
    }

    Ok(domains)
}
