use std::{
    collections::HashMap,
    task::{self, Poll},
};

use anyhow::Context;
use futures::future::poll_fn;

mod backends;
mod triggers;

#[derive(serde::Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Config {
    #[serde(default, rename = "trigger")]
    triggers: HashMap<String, toml::Value>,
    #[serde(default, rename = "backend")]
    backends: HashMap<String, toml::Value>,
}

pub enum Action {
    TurnDndOn,
    TurnDndOff,
}

pub trait Trigger {
    fn from_config(config: toml::Value) -> anyhow::Result<Self>
    where
        Self: Sized;

    fn poll(&mut self, cx: &mut task::Context) -> Poll<anyhow::Result<Action>>;
}

#[async_trait::async_trait]
pub trait Backend {
    fn from_config(config: toml::Value) -> anyhow::Result<Self>
    where
        Self: Sized;

    async fn enter(&self) -> anyhow::Result<()>;
    async fn leave(&self) -> anyhow::Result<()>;
}

#[derive(Default)]
pub struct Daemon {
    triggers: Vec<Box<dyn Trigger>>,
    backends: Vec<Box<dyn Backend>>,
}

macro_rules! generate_typemap {
    ($source:expr => $vec:expr => { $($name:literal: $typ:ty,)*}) => {{
        for (name, config) in $source {
            let item = match &name as &str {
                $($name => {
                    let config = <$typ>::from_config(config).with_context(|| format!("parsing config for {name}"))?;
                    Box::new(config)
                }),*
                _ => anyhow::bail!("No config named {} known to dnd", name),
            };
            $vec.push(item);
        }
    }}
}

impl Config {
    pub async fn into_daemon(self) -> anyhow::Result<Daemon> {
        let mut daemon = Daemon::default();

        generate_typemap!(self.triggers => daemon.triggers => {
            "network": triggers::Network,
        });

        generate_typemap!(self.backends => daemon.backends => {
            "mako": backends::Mako,
        });

        Ok(daemon)
    }
}

impl Daemon {
    pub async fn process_event(&mut self) -> anyhow::Result<()> {
        if self.triggers.is_empty() {
            anyhow::bail!("No triggers declared, aborting.")
        }
        let action = poll_fn(|cx| {
            for trigger in &mut self.triggers {
                match trigger.poll(cx) {
                    Poll::Ready(x) => return Poll::Ready(x),
                    Poll::Pending => (),
                }
            }
            Poll::Pending
        })
        .await?;

        for backend in &mut self.backends {
            match action {
                Action::TurnDndOn => {
                    backend.enter().await?;
                }
                Action::TurnDndOff => {
                    backend.leave().await?;
                }
            }
        }

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn deserialize_some_configs() {
        let configs = [toml::toml! {
        [backend.mako]
        dnd-mode = "do-not-disturb"
        default-mode = "default"
        }];

        for config in configs {
            let _: Config = config.try_into().unwrap();
        }
    }
}
