# Overengineered Do-Not-Disturb daemon

Do-not-disturb is capable of using multiple clues [1] in order to silence or re-enable multiple [2] notification systems.

For instance, I use it to disable `mako` while I'm at work.
This is my config file:

```toml
[trigger.network]
dnd_search_domains = ["etrovub.be", "vub.ac.be"]

[backend.mako]
```

When I'm at work, I get either `etrovub.be` or `vub.ac.be` as search domain through DHCP.
`dnd` acts on this, and enables or disables `mako` through its "mode" setting.

I plan on having an override system through a `dnd` command too,
and patches are welcome for other triggers and/or backends.

# Installation

Install with `cargo`, for now:

```sh
cargo install --git https://gitlab.com/rubdos/do-not-disturb.git
```

# Configuration

Put your config file at `~/.config/dnd/config.toml`.

```toml
[trigger.network]
dnd_search_domains = ["workdomain.example.com"]

[backend.mako]
```

[1]: Only one trigger currently implemented
[2]: Only one notification system implemented
